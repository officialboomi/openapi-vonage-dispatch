# Vonage Dispatch Connector
The Messages API allows you to send and in some cases receive messages over SMS, MMS, Facebook Messenger, Viber, and WhatsApp. Further channels may be supported in the future.

Documentation: https://developer.vonage.com/dispatch/overview

## Prerequisites

+ Vonage account
+ API Key and Secret for Basic Authentication

## Supported Operations
**All endpoints are operational.**
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

